//
//  main.m
//  TestBlur
//
//  Created by Diego Lima on 27/05/14.
//  Copyright (c) 2014 Pontomobi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TestAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TestAppDelegate class]));
    }
}
