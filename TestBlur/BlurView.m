//
//  BlurView.m
//  TestBlur
//
//  Created by Diego Lima on 28/05/14.
//  Copyright (c) 2014 Pontomobi. All rights reserved.
//

#import "BlurView.h"
#import "UIImage+Effects.h"

@interface BlurView ()

@property (nonatomic, strong) UIImageView *view1;
@property (nonatomic, strong) UIImageView *view2;
@property (nonatomic, strong) UIImageView *view3;
@property (nonatomic, strong) UIImageView *view4;

@end

@implementation BlurView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor darkGrayColor]];
        
        UIImage *image1 = [UIImage imageNamed:@"Image1.jpg"];
        UIImage *image2 = [UIImage imageNamed:@"Image2.jpg"];
        UIImage *image3 = [UIImage imageNamed:@"Image3.jpg"];
        UIImage *image4 = [UIImage imageNamed:@"Image4.jpg"];

        image1 = [image1 blurEffect:28 byFrame:CGRectMake(0.0f, image1.size.height/2, image1.size.width, image1.size.height/2) withGradient:UIImageGradientDirectionBottomToTop];
        
        image2 = [image2 blurEffect:20 byFrame:CGRectMake(0.0f, 0.0f, image2.size.width, image2.size.height) withGradient:UIImageGradientDirectionTopToBottom];

        image3 = [image3 blurEffect:15 byFrame:CGRectMake(0.0f, image3.size.height/2, image3.size.width, image3.size.height/2)];
        
        image4 = [image4 blurEffect:4 byFrame:CGRectMake(0.0f, 0.0f, image4.size.width, image4.size.height)];
        

        self.view1 = [[UIImageView alloc] initWithImage:image1];
        [self addSubview:self.view1];
        
        self.view2 = [[UIImageView alloc] initWithImage:image2];
        [self addSubview:self.view2];
        
        self.view3 = [[UIImageView alloc] initWithImage:image3];
        [self addSubview:self.view3];
        
        self.view4 = [[UIImageView alloc] initWithImage:image4];
        [self addSubview:self.view4];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.view1.center = CGPointMake(self.frame.size.width/4, self.frame.size.height/4);
    self.view2.center = CGPointMake(self.frame.size.width - (self.frame.size.width/4), self.frame.size.height/4);
    self.view3.center = CGPointMake(self.frame.size.width/4, self.frame.size.height - (self.frame.size.height/4));
    self.view4.center = CGPointMake(self.frame.size.width - (self.frame.size.width/4), self.frame.size.height - (self.frame.size.height/4));

}

@end
