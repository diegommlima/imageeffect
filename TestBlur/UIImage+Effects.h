//
//  UIImage+Effects.h
//  TestBlur
//
//  Created by Diego Lima on 27/05/14.
//  Copyright (c) 2014 Pontomobi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum __UIImageGradientDirection
{
    UIImageGradientDirectionTopToBottom  = 1,
    UIImageGradientDirectionBottomToTop  = 2,
    UIImageGradientDirectionLeftToRight  = 3,
    UIImageGradientDirectionRightToLeft  = 4

} UIImageGradientDirection;

@interface UIImage (Effects)


- (instancetype)croppedImageAtFrame:(CGRect)frame;
- (instancetype)imageAddingImage:(UIImage*)image offset:(CGPoint)offset;
- (instancetype)blurEffect:(CGFloat)blurRatio byFrame:(CGRect)frame;
- (instancetype)blurEffect:(CGFloat)blurRatio byFrame:(CGRect)frame withGradient:(UIImageGradientDirection)direction;
- (instancetype)imageToGradient:(UIImage *)image size:(CGSize)size direction:(UIImageGradientDirection)direction;

@end
